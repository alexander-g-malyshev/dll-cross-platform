
#include "testone.h"
#include <iostream>

START_MODULE(testmodule, testone)

testone::testone()
{
  std::cout << "function testone::testone() is called" << std::endl;
}

testone::~testone()
{
  std::cout << "function testone::~testone() is called" << std::endl;
}

int testone::start(void *pdata)
{
  std::cout << "function testone::start(...) is called" << std::endl;
  return 0;
}

END_MODULE()
