#ifndef _TEST_MODULE_15_JAN_2010_
#define _TEST_MODULE_15_JAN_2010_

#include "loadlibrarymacros.h"
#include "dllbase.h"

START_MODULE_DECLARATION(testmodule, testone)

class testone : public dllbase
{
  public:
   testone();
   virtual ~testone();
   
  public:
    virtual int start(void *pdata);
};

END_MODULE_DECLARATION()

#endif //_TEST_MODULE_15_JAN_2010_
