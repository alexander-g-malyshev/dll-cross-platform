#ifndef _TEST_MODULE_TWO_15_JAN_2010_
#define _TEST_MODULE_TWO_15_JAN_2010_

#include "loadlibrarymacros.h"
#include "dllbase.h"

START_MODULE_DECLARATION_2(testmodule, testmoduleone, testtwo)

class testtwo : public dllbase
{
  public:
   testtwo();
   virtual ~testtwo();

  public:
    virtual int start(void *pdata);
};

END_MODULE_DECLARATION()

#endif //_TEST_MODULE_TWO_15_JAN_2010_
