
#include "testtwo.h"
#include <iostream>

START_MODULE_2(testmodule, testmoduleone, testtwo)

testtwo::testtwo()
{
  std::cout << "function testtwo::testtwo() is called" << std::endl;
}

testtwo::~testtwo()
{
  std::cout << "function testtwo::~testtwo() is called" << std::endl;
}

int testtwo::start(void *pdata)
{
  std::cout << "function testtwo::start(...) is called" << std::endl;
  return 0;
}

END_MODULE()
