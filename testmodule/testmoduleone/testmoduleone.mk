
TEST_MODULE_ONE_DIR=$(HOME_DIR)/testmodule/testmoduleone
TEST_MODULE_ONE_DIR_BIN=$(HOME_DIR)/bin/testmodule/testmoduleone

LIB_NAME=lib

TARGETS_TEST_MODULE_ONE=\
	testtwo

build_testmodule_two: $(TARGETS_ENGINE) $(TARGETS_TEST_MODULE_ONE)

#	$(CC) $(DEBUG) $(CFLAGS) $(DEFINE) $(INCLUDE_DIR) -c $(HOME_DIR)/$@.cpp -o $(BIN_DIR)/$@.o


$(TARGETS_TEST_MODULE_ONE):
	$(CC) $(DEBUG) -fPIC $(CFLAGS) $(DEFINE) $(INCLUDE_DIR) -c $(TEST_MODULE_ONE_DIR)/$@.cpp -o $(TEST_MODULE_ONE_DIR_BIN)/$@.o
	$(CC) $(DEBUG) -shared -ldl $(LIB_DIR) $(TEST_MODULE_ONE_DIR_BIN)/$@.o $(ENGINE_DIR_BIN)/*.o -o $(TEST_MODULE_ONE_DIR_BIN)/$@.$(DYNAMIC_MODULE_EXT)

