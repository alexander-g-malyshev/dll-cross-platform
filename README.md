The library give interface to work with DLL(Windows, Linux)

The library was tested on the Linux(Fedora 13), Windows.

Available options to build project

all - build project

static - create static or dynamic project. Available values: on, off.

platform - type of the platform. Available types: linux

digit - digit of the platform

debug information is on - debug=on

debug information is off - debug=off

example: make all platform=linux digit=64 debug=on

DEFAULT TARGETS for 'all' are platform=linux, digit=64, debug=on

build project example:

make build_engine platform=linux digit=64 debug=on build engine target

make build_testmodule platform=linux digit=64 debug=on build test target

make all platform=linux digit=64 debug=on static=on (build and create executable file) in this case .so library is not needed

make all platform=linux digit=64 debug=on (build and create executable file) in this case .so library needed

document: function specification API is not ready. the document will be created later.

example: examples can be found in the source code 