
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/

#ifndef _DLL_BASE_H_12_01_2010_
#define _DLL_BASE_H_12_01_2010_


class dllbase
{
  public:
   dllbase();
   virtual ~dllbase();

 public:  
  virtual int start(void *pdata) = 0;
};

//typedef dllbase* (*maker_t)();

#endif /* _DLL_BASE_H_12_01_2010_ */

