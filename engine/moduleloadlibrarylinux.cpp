
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/



#include "moduleloadlibrarylinux.h"
#include <dlfcn.h>
#include <limits.h>

CModuleLoadLibraryLinux::CModuleLoadLibraryLinux()
{
}

CModuleLoadLibraryLinux::~CModuleLoadLibraryLinux()
{
}

void* CModuleLoadLibraryLinux::Open(std::string path)
{
  return dlopen(path.c_str(), RTLD_NOW);
}

int CModuleLoadLibraryLinux::Close(void *handle)
{
  return (handle != NULL) ? dlclose(handle) : NULL;
}
   
void* CModuleLoadLibraryLinux::Load(void *handle, std::string symbol)
{
  return dlsym(handle, symbol.c_str());
}

std::string CModuleLoadLibraryLinux::getErrorDescription()
{
  return std::string(dlerror());
}

const char CModuleLoadLibraryLinux::getPathSeparator()
{
  return '/';
}

std::string CModuleLoadLibraryLinux::getexepath()
{
 char result[ PATH_MAX ];
 ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
 return std::string( result, (count > 0) ? count : 0 );
}

std::string CModuleLoadLibraryLinux::getModuleNameExt()
{
  return std::string("lso");
}

