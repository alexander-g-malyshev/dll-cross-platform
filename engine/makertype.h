
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/


#ifndef _LOAD_LIBRARY_MAKER_TYPE_15_01_2010_
#define _LOAD_LIBRARY_MAKER_TYPE_15_01_2010_


class dllbase;

#ifndef WINDOWS
typedef void* (*maker_t)();
#else
#include <windows.h>
typedef void* (WINAPI*maker_t)();
#endif

#endif /* _LOAD_LIBRARY_MAKER_TYPE_15_01_2010_ */


