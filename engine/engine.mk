
ENGINE_DIR=$(HOME_DIR)/engine
ENGINE_DIR_BIN=$(HOME_DIR)/bin/engine

TARGETS_ENGINE=\
	baseobject\
	loadlibraryproxy\
	moduleloadlibrary\
	moduleloadlibrarylinux\
	dllbase


build_engine: $(TARGETS_ENGINE)
#	$(CC) -W -Wall -ldl  -fPIC $(LFLAGS) $(LIB_DIR) $(ENGINE_DIR_BIN)/*.o -o $(BIN_DIR)/main_test$(PLATFORM_DIGIT)$(OUT_EXT)


$(TARGETS_ENGINE):
	$(CC) $(DEBUG) $(CFLAGS) -fPIC $(DEFINE) $(INCLUDE_DIR) -c $(ENGINE_DIR)/$@.cpp -o $(ENGINE_DIR_BIN)/$@.o
