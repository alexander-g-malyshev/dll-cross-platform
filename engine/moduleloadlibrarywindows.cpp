
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/


#include "moduleloadlibrarywindows.h"
#include <windows.h>
#include <winbase.h>
#include <tchar.h>
#include <direct.h>

CModuleLoadLibraryWindows::CModuleLoadLibraryWindows()
{
}

CModuleLoadLibraryWindows::~CModuleLoadLibraryWindows()
{
}

void* CModuleLoadLibraryWindows::Open(std::string path)
{
  return reinterpret_cast<void*>(::LoadLibrary(_T(path.c_str())));
}

int CModuleLoadLibraryWindows::Close(void *handle)
{
  return ::FreeLibrary(reinterpret_cast<HMODULE>(handle));
}
   
void* CModuleLoadLibraryWindows::Load(void *handle, std::string symbol)
{
  return (void*)(::GetProcAddress(reinterpret_cast<HMODULE>(handle),
                            symbol.c_str()));
}

std::string CModuleLoadLibraryWindows::getModuleNameExt()
{
  return std::string("dll");
}

const char CModuleLoadLibraryWindows::getPathSeparator()
{
  return '\\';
}


std::string CModuleLoadLibraryWindows::getErrorDescription()
{
  return std::string("stub error description");
}

std::string CModuleLoadLibraryWindows::getexepath()
{
  char result[ MAX_PATH ];
  std::string ret_string;
  int         icount = 0;
  int         str_len;
  
  GetModuleFileName( NULL, result, MAX_PATH );

  str_len = strlen(result);
  icount = str_len-1;
  
  while(result[icount] != getPathSeparator() && icount > 0)
   icount --;

  ret_string = result;
  return ret_string;
}



