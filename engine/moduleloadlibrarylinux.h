
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/


#ifndef _MODULE_LOAD_LIBRARY_LINUX_15_01_2010_
#define _MODULE_LOAD_LIBRARY_LINUX_15_01_2010_

#include "moduleloadlibrary.h"

class CModuleLoadLibraryLinux : public CModuleLoadLibrary
{
  public:
    CModuleLoadLibraryLinux();
    virtual ~CModuleLoadLibraryLinux();

   virtual void* Open(std::string path);
   virtual int Close(void *handle);
   
   virtual void* Load(void *handle, std::string symbol);
   virtual std::string getModuleNameExt();
   virtual const char getPathSeparator();

   std::string getErrorDescription();
   std::string getexepath();

};

#endif /* _MODULE_LOAD_LIBRARY_LINUX_15_01_2010_ */


