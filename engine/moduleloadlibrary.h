
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/

#ifndef _MODULE_LOAD_LIBRARY_15_01_2010_
#define _MODULE_LOAD_LIBRARY_15_01_2010_

#include <string>

class CModuleLoadLibrary
{
  public:
   CModuleLoadLibrary();
   virtual ~CModuleLoadLibrary();

   virtual void *Open(std::string path) = 0;
   virtual int Close(void *handle) = 0;
   
   virtual void* Load(void *handle, std::string symbol) = 0;
   virtual std::string getErrorDescription() = 0;
   
   virtual std::string getexepath() =0;

   virtual std::string getModuleNameExt() =0;
   virtual const char getPathSeparator() =0;
   
  protected:
    std::string  librarypath;   
};

#endif /* _MODULE_LOAD_LIBRARY_15_01_2010_ */

