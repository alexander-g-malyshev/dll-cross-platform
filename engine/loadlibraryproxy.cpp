
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/


#include "loadlibraryproxy.h"
#include "makertype.h"

#ifdef LINUX
#include <limits.h>
#include <unistd.h>
#elif defined WINDOWS
#include <windows.h>
#include <direct.h>
#else /* NIX */
#include <limits.h>
#define _PSTAT64
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>



CLoadLibraryProxy* CLoadLibraryProxy::pLoadLibraryProxy = NULL;
unsigned long CLoadLibraryProxy::CLoadLibraryProxyCounter  = 0;

CLoadLibraryProxy::CLoadLibraryProxy()
{
  dl_list.clear();
}

CLoadLibraryProxy::CLoadLibraryProxy(const CLoadLibraryProxy& prx)
{
  pmoduleloader = prx.pmoduleloader;
  basepath      = prx.basepath;
  dl_list       = prx.dl_list;
}

CLoadLibraryProxy::~CLoadLibraryProxy()
{
  if(pmoduleloader)
  {
     delete pmoduleloader;
     pmoduleloader = NULL;
  }
}

CLoadLibraryProxy CLoadLibraryProxy::operator=(const CLoadLibraryProxy& prx)
{
  pmoduleloader = prx.pmoduleloader;
  basepath      = prx.basepath;
  dl_list       = prx.dl_list;
  
  return (*this);
}

unsigned long CLoadLibraryProxy::DeleteLoadLibraryProxy()
{
  if(!(CLoadLibraryProxyCounter--))
  {
    CLoadLibraryProxyCounter = 0;
    delete this;
  }
  return CLoadLibraryProxyCounter;
}

bool CLoadLibraryProxy::IsCreatedLoadLibraryProxy()
{
   if(CLoadLibraryProxyCounter)
     return true;
   else
     return false;
}

void CLoadLibraryProxy::setModuleLoadLibrary( CModuleLoadLibrary * moduleloader)
{
  pmoduleloader = moduleloader;
}

CModuleLoadLibrary *CLoadLibraryProxy::getModuleLoadLibrary()
{
  return pmoduleloader;
}

void CLoadLibraryProxy::setBasePathLibrary(std:: string basepathlibrary)
{
  basepath = basepathlibrary;
}

std::string CLoadLibraryProxy::getBasePathLibrary()
{
  return basepath;
}

void *CLoadLibraryProxy::findObject(std:: string snamespace, std::string sobjectname)
{
  std::string   namespase_path   = convertNamespaceToPath(snamespace);
  std::string   path             = basepath + getPathSeparator() + namespase_path  +
                                     getPathSeparator() + sobjectname + "." + getModuleNameExt();
  std::string   modulename       = snamespace + "_" + sobjectname + "_maker_t";
  void         *mkr              = NULL;
  maker_t       mkfunc           = NULL;
  void         *loadedobj        = NULL;
  ObjectInfo    lObjectInfo;
  void *phandle                  = pmoduleloader->Open(path);

  if(phandle == NULL) return NULL;

  lObjectInfo.handle = phandle;
  lObjectInfo.name  = modulename;
  dl_list.insert(dl_list.end(), lObjectInfo);

  mkr = pmoduleloader->Load(phandle, modulename);
  if(mkr == NULL) return NULL;

  mkfunc = (maker_t)(mkr);

  loadedobj = (*mkfunc)();
  return loadedobj;
}


const char CLoadLibraryProxy::getPathSeparator()
{
  return pmoduleloader->getPathSeparator();
}

const char CLoadLibraryProxy::getNameSpaceSeparator()
{
  return '_';
}

/*
 * convert namespace to path. (namespace define path to the library)
 * input format namespase: name1.name2.name3. and so on
 * result: name1/name2/name3/...
 *         result depends on operation system
 */
std::string CLoadLibraryProxy::convertNamespaceToPath(std:: string snamespace)
{
  std::string::iterator it;
  const char separator = getNameSpaceSeparator();

  for ( it=snamespace.begin() ; it < snamespace.end(); it++ )
  {
    if(*it == separator)
    {
      snamespace.erase(it);
      snamespace.insert(it, getPathSeparator());
    }
  }

  return snamespace;
}

void CLoadLibraryProxy::deleteObject(std:: string snamespace, std::string sobjectname)
{
  std::string                     modulename = snamespace + "_" + sobjectname + "_maker_t";
  std::list<ObjectInfo>::iterator dl_list_iterator;

  for(dl_list_iterator = dl_list.begin(); dl_list_iterator != dl_list.end(); dl_list_iterator ++)
  {
    if(dl_list_iterator->name.compare(modulename) == 0)
    {
      pmoduleloader->Close(dl_list_iterator->handle);
      dl_list.erase(dl_list_iterator);
      break;
    }
  }
}

std::string CLoadLibraryProxy::getexedir()
{
  std::string path = getexepath();
  const char c     = getPathSeparator();
  std::string::iterator it;

  for ( it = path.end()-1 ; it != path.begin(); it-- )
  {
    if((*it) == c) break;
  }

  path.erase(it, path.end());
  return path;
}

const char* CLoadLibraryProxy::get_directory( char* path )
{
#if defined (WINDOWS)
  const char *c = "\\";
#else
  const char* c = "/";
#endif
  getcwd( path, 260 );
  strcat( path, c );
  return path;
}

std::string CLoadLibraryProxy::getexepath()
{
  return pmoduleloader->getexepath();
}

std::string CLoadLibraryProxy::getModuleNameExt()
{
  return pmoduleloader->getModuleNameExt();
}




