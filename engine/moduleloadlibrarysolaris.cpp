/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/



#include "moduleloadlibrarysolaris.h"
#include <dlfcn.h>
#include <unistd.h>
#include <limits.h>

CModuleLoadLibrarySolaris::CModuleLoadLibrarySolaris()
{
}

CModuleLoadLibrarySolaris::~CModuleLoadLibrarySolaris()
{
}

void* CModuleLoadLibrarySolaris::Open(std::string path)
{
  return dlopen(path.c_str(), RTLD_NOW);
}

int CModuleLoadLibrarySolaris::Close(void *handle)
{
  return (handle != NULL) ? dlclose(handle) : NULL;
}
   
void* CModuleLoadLibrarySolaris::Load(void *handle, std::string symbol)
{
  return dlsym(handle, symbol.c_str());
}

std::string CModuleLoadLibrarySolaris::getErrorDescription()
{
  return std::string(dlerror());
}

const char CModuleLoadLibrarySolaris::getPathSeparator()
{
  return '/';
}

std::string CModuleLoadLibrarySolaris::getexepath()
{
  char *result = (char*)getexecname();
  std::string ret_string;
  int str_len = strlen(result);
  int icount = str_len-1;

   while(result[icount] != getPathSeparator() && icount > 0)
   icount --;

  result[icount] = '\0';
  ret_string = result;
  ret_string += getPathSeparator();
  return ret_string;
}
#if 0

std::string CModuleLoadLibrarySolaris::getexepath()
{

  char result_cwd[PATH_MAX];
  char *result = (char*)getexecname();
  std::string ret_string = result;
  int str_len = strlen(result);
  int icount = str_len-1;
  
  while(result[icount] != getPathSeparator() && icount > 0)
   icount --;

  result[icount] = '\0';

  getcwd(result_cwd, PATH_MAX);
  ret_string = result_cwd;
  ret_string += getPathSeparator();

  printf("cwd result: %s\n", result_cwd);
  printf("execname result: %s\n", result);
  

  if(ret_string != std::string(result))
    ret_string += result;

  
  ret_string += getPathSeparator();
  
  if(ret_string.at(ret_string.length()-1) != getPathSeparator())
    ret_string += getPathSeparator();

  printf("return result: %s\n", ret_string.c_str());
  return ret_string;
}
#endif

std::string CModuleLoadLibrarySolaris::getModuleNameExt()
{
  return std::string("sso");
}



