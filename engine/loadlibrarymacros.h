
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/

#ifndef _LAD_LIBRARY_MACROS_18_01_2010_
#define _LAD_LIBRARY_MACROS_18_01_2010_


#ifdef WINDOWS

#define START_MODULE_DECLARATION(name_space, class_name)\
class class_name;\
extern "C" {\
 __declspec(dllexport) class_name * name_space##_##class_name##_maker_t();\
};\
 \
namespace name_space \
{


#define END_MODULE_DECLARATION()\
};

#define START_MODULE(name_space, class_name)\
namespace name_space\
{\
  extern "C" {\
    __declspec(dllexport) class_name * name_space##_##class_name##_maker_t()\
    {\
      return new name_space::class_name();\
    }\
  }\


#define END_MODULE()\
};

#else  //NOT FOR MSVS

#define START_MODULE_DECLARATION(name_space, class_name)\
class class_name;\
extern "C" {\
class_name * name_space##_##class_name##_maker_t();\
}\
 \
namespace name_space \
{\


#define END_MODULE_DECLARATION()\
}

#define START_MODULE(name_space, class_name)\
class_name * name_space##_##class_name##_maker_t()\
{\
  return reinterpret_cast<class_name*>(new name_space::class_name());\
}\
\
namespace name_space\
{\


#define END_MODULE()\
}

#endif //WINDOWS



#define START_MODULE_DECLARATION_1(name_space, class_name) START_MODULE_DECLARATION(name_space, class_name)

#define START_MODULE_DECLARATION_2(name_space0, name_space1, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1, class_name)

#define START_MODULE_DECLARATION_3(name_space0, name_space1, name_space2, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2, class_name)

#define START_MODULE_DECLARATION_4(name_space0, name_space1, name_space2, name_space3, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3, class_name)

#define START_MODULE_DECLARATION_5(name_space0, name_space1, name_space2, name_space3, name_space4, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4, class_name)

#define START_MODULE_DECLARATION_6(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5, class_name)

#define START_MODULE_DECLARATION_7(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6, class_name)

#define START_MODULE_DECLARATION_8(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7, class_name)

#define START_MODULE_DECLARATION_9(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, name_space8, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##name_space8, class_name)

#define START_MODULE_DECLARATION_10(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, name_space8, name_space9, class_name)\
  START_MODULE_DECLARATION(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##name_space8##_##name_space9, class_name)





#define START_MODULE_1(name_space, class_name) START_MODULE(name_space, class_name)

#define START_MODULE_2(name_space0, name_space1, class_name)\
  START_MODULE(name_space0##_##name_space1, class_name)

#define START_MODULE_3(name_space0, name_space1, name_space2, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2, class_name)

#define START_MODULE_4(name_space0, name_space1, name_space2, name_space3, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2_##name_space3, class_name)

#define START_MODULE_5(name_space0, name_space1, name_space2, name_space3, name_space4, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2##_##name_space3##_##name_space4, class_name)

#define START_MODULE_6(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2_##name_space3##_##name_space4##_##name_space5, class_name)

#define START_MODULE_7(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2_##name_space3##_##name_space4##_##name_space5##_##name_space6, class_name)

#define START_MODULE_8(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7, class_name)

#define START_MODULE_9(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, name_space8, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##name_space8, class_name)

#define START_MODULE_10(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, name_space8, name_space9, class_name)\
  START_MODULE(name_space0##_##name_space1##_##name_space2_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##name_space8##_##name_space9, class_name)



#define GET_MODULE_FUNCTION_MAKER_NAME(name_space, class_name) name_space##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_2(name_space0, name_space1, class_name) name_space##_##name_space1##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_3(name_space0, name_space1, name_space2, class_name) name_space##_##name_space1##_##name_space2##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_4(name_space0, name_space1, name_space2, name_space3, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_5(name_space0, name_space1, name_space2, name_space3, name_space4, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_6(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_7(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_8(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_9(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, name_space8, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##name_space8##_##class_name##_maker_t
#define GET_MODULE_FUNCTION_MAKER_NAME_10(name_space0, name_space1, name_space2, name_space3, name_space4, name_space5, name_space6, name_space7, name_space8, name_space9, class_name) name_space##_##name_space1##_##name_space2##_##name_space3##_##name_space4##_##name_space5##_##name_space6##_##name_space7##_##name_space8##_##name_space9##_##class_name##_maker_t

#endif //_LAD_LIBRARY_MACROS_18_01_2010_

