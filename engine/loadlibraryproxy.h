
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/


#ifndef _LOAD_LIBRARY_PROXY_15_01_2010_
#define _LOAD_LIBRARY_PROXY_15_01_2010_

#include "moduleloadlibrary.h"
#include <string>
#include <list>

typedef struct _tag_ObjectInfo
{
  std::string   name;
  void         *handle;

}ObjectInfo;

class CLoadLibraryProxy
{
  public:
    CLoadLibraryProxy();
    CLoadLibraryProxy(const CLoadLibraryProxy& prx);
    virtual ~CLoadLibraryProxy();

    static CLoadLibraryProxy* CreateLoadLibraryProxy()
    {
      if(!pLoadLibraryProxy)
      {
         if((pLoadLibraryProxy = new(CLoadLibraryProxy)))
         {
            CLoadLibraryProxyCounter ++;
            return pLoadLibraryProxy;
         }
         else
         {
            return NULL;
         }
       }
       else
       {
         CLoadLibraryProxyCounter++;
         return pLoadLibraryProxy;
       }
    };

    
    static CLoadLibraryProxy* GetLoadLibraryProxyPointer( )
    {
      return pLoadLibraryProxy;
    };


    virtual unsigned long DeleteLoadLibraryProxy();
    static bool IsCreatedLoadLibraryProxy();


    void setModuleLoadLibrary( CModuleLoadLibrary * moduleloader);
    CModuleLoadLibrary *getModuleLoadLibrary();


    void setBasePathLibrary(std:: string basepathlibrary);
    std::string getBasePathLibrary();

    
    void *findObject(std:: string snamespace, std::string sobjectname);
    void deleteObject(std:: string snamespace, std::string sobjectname);

    std::string getexepath();
    std::string getexedir();

    const char* get_directory( char* path );

    const char getPathSeparator();
    const char getNameSpaceSeparator();
    
    std::string getModuleNameExt();

    
   CLoadLibraryProxy operator=(const CLoadLibraryProxy& prx);

protected:
    std::string convertNamespaceToPath(std::string snamespace);

  protected:
   CModuleLoadLibrary          *pmoduleloader;
   std::string                  basepath;
   std::list<ObjectInfo>        dl_list;


  protected:
   static CLoadLibraryProxy* pLoadLibraryProxy;
   static unsigned long CLoadLibraryProxyCounter;
};

#endif /* _LOAD_LIBRARY_PROXY_15_01_2010_ */



