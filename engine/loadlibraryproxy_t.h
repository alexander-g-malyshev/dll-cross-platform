  
/*****************************************************************************
 * Code license:  GNU General Public License v3                              *
 * Main page:     http://code.google.com/p/dll-cross-platform/               *
 * Issue tracker: http://code.google.com/p/dll-cross-platform/issues/list    *
*****************************************************************************/


#ifndef _LOAD_LIBRARY_PROXY_T_23_06_2011_
#define _LOAD_LIBRARY_PROXY_T_23_06_2011_


#include "loadlibraryproxy.h"

template <class T>
class CLoadLibraryProxyT : public CLoadLibraryProxy
{
  public:
    static CLoadLibraryProxyT* CreateLoadLibraryProxyT()
    {
      if(!pLoadLibraryProxy)
      {
         if((pLoadLibraryProxy = new(CLoadLibraryProxyT)))
         {
            CLoadLibraryProxyCounter ++;
            return (CLoadLibraryProxyT<T>*)pLoadLibraryProxy;
         }
         else
         {
            return NULL;
         }
       }
       else
       {
         CLoadLibraryProxyCounter++;
         return (CLoadLibraryProxyT<T>*)pLoadLibraryProxy;
       }
    };

		
    T *getObject(std:: string snamespace, std::string sobjectname) {
      return static_cast<T*>( findObject(snamespace, sobjectname) );
    }
};

#endif //_LOAD_LIBRARY_PROXY_T_23_06_2011_