#include <iostream>
#include "loadlibraryproxy.h"
#include "loadlibraryproxy_t.h"
#include "dllbase.h"

#if !defined WINDOWS && !defined STATIC_PROJECT
#include <dlfcn.h>
#include "moduleloadlibrarylinux.h"
#elif defined WINDOWS && !defined STATIC_PROJECT
#include <windows.h>
#include "moduleloadlibrarywindows.h"
#endif

#ifdef STATIC_PROJECT
#include "testone.h"
#endif


static int load_test_01();
static int load_test_02();

int main(int argc, char** argv)
{
  std::cout << "######## START TEST 01 #########"  << std::endl;
  (void)load_test_01();
  std::cout << "######## FINISH TEST 01 #########"  << std::endl;
	
  std::cout << "######## START TEST 02 [Template used instead runtime cast] #########"  << std::endl;
  (void)load_test_02();
  std::cout << "######## FINISH TEST 02 #########"  << std::endl;
}


int load_test_01()
{
  dllbase   *ptest = NULL;
  std::string namespace_module;

#ifndef STATIC_PROJECT
  CLoadLibraryProxy *pproxy = CLoadLibraryProxy::CreateLoadLibraryProxy();

#ifndef WINDOWS
  pproxy->setModuleLoadLibrary(new CModuleLoadLibraryLinux());
#else
  pproxy->setModuleLoadLibrary(new CModuleLoadLibraryWindows());
#endif

  pproxy->setBasePathLibrary(pproxy->getexedir());


/* Load testone from testmodule */
  namespace_module = "testmodule";
  ptest = static_cast<dllbase*>(pproxy->findObject(namespace_module, "testone"));
  
  if(ptest)
  {
    ptest->start(NULL);
    delete ptest; ptest = NULL;
  }
  else
  {
    std::cerr << pproxy->getModuleLoadLibrary()->getErrorDescription()  << std::endl;
    return -1;
  }


/* Load testone from testmodule.testmoduleone */
  namespace_module = "testmodule";
  namespace_module += pproxy->getNameSpaceSeparator();
  namespace_module += "testmoduleone";
  ptest = static_cast<dllbase*>(pproxy->findObject(namespace_module, "testtwo"));

  if(ptest)
  {
    ptest->start(NULL);
    delete ptest; ptest = NULL;
  }
  else
  {
    std::cerr << pproxy->getModuleLoadLibrary()->getErrorDescription()  << std::endl;
    return -1;
  }


namespace_module = "testmodule";
  pproxy->deleteObject(namespace_module, "testone");

namespace_module = "testmodule";
namespace_module += pproxy->getNameSpaceSeparator();
namespace_module += "testmoduleone";

  pproxy->deleteObject(namespace_module, "testtwo");
#else
  ptest = new testmodule::testone();
  if(ptest)
  {
    ptest->start(NULL);
    delete ptest; ptest = NULL;
  }
  else
  {
    std::cerr << "pointer is NULL"  << std::endl;
    return -1;
  }
#endif
  return 0;
}


int load_test_02()
{
  dllbase   *ptest = NULL;
  std::string namespace_module;

#ifndef STATIC_PROJECT
  CLoadLibraryProxyT<dllbase> *pproxy = CLoadLibraryProxyT<dllbase>::CreateLoadLibraryProxyT();

#ifndef WINDOWS
  pproxy->setModuleLoadLibrary(new CModuleLoadLibraryLinux());
#else
  pproxy->setModuleLoadLibrary(new CModuleLoadLibraryWindows());
#endif

  pproxy->setBasePathLibrary(pproxy->getexedir());


/* Load testone from testmodule */
  namespace_module = "testmodule";
  ptest = pproxy->getObject(namespace_module, "testone");
  
  if(ptest)
  {
    ptest->start(NULL);
    delete ptest; ptest = NULL;
  }
  else
  {
    std::cerr << pproxy->getModuleLoadLibrary()->getErrorDescription()  << std::endl;
    return -1;
  }


/* Load testone from testmodule.testmoduleone */
  namespace_module = "testmodule";
  namespace_module += pproxy->getNameSpaceSeparator();
  namespace_module += "testmoduleone";
  ptest = static_cast<dllbase*>(pproxy->findObject(namespace_module, "testtwo"));

  if(ptest)
  {
    ptest->start(NULL);
    delete ptest; ptest = NULL;
  }
  else
  {
    std::cerr << pproxy->getModuleLoadLibrary()->getErrorDescription()  << std::endl;
    return -1;
  }


namespace_module = "testmodule";
  pproxy->deleteObject(namespace_module, "testone");

namespace_module = "testmodule";
namespace_module += pproxy->getNameSpaceSeparator();
namespace_module += "testmoduleone";

  pproxy->deleteObject(namespace_module, "testtwo");
#else
  ptest = new testmodule::testone();
  if(ptest)
  {
    ptest->start(NULL);
    delete ptest; ptest = NULL;
  }
  else
  {
    std::cerr << "pointer is NULL"  << std::endl;
    return -1;
  }
#endif
  return 0;
}

