######################################################################
###################### COMMENTS ######################################
######################################################################

# Environment
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
RANLIB=ranlib


CC=g++
LINKER=ld
CFLAGS=-pipe
LFLAGS=-lpthread -ldl
DLLFLAGS=-fPIC -shared
PLATFORM=linux
PLATFORM_DIGIT=64

HOME_DIR=.
BIN_DIR=$(HOME_DIR)/bin

DEFINE=
OUT_EXT=
EXE_FILE_TYPE=
DYNAMIC_MODULE_EXT=lso

		
include $(HOME_DIR)/Version.mk
include $(HOME_DIR)/Includedir.mk

ifeq ($(platform), linux)
	DEFINE += -DLINUX
	PLATFORM = linux
	OUT_EXT=
endif
ifeq ($(platform), win)
	DEFINE += -DWINDOWS
	PLATFORM = win
	OUT_EXT = .exe
endif

ifeq ($(digit), 32)
	PLATFORM_DIGIT = 32
	CFLAGS+=-m32
else
	PLATFORM_DIGIT = 64
	CFLAGS+=-m64
endif

ifeq ($(platform), linux)
ifneq ($(static),on)
	DLLFLAGS += -shared -fPIC
endif
ifeq ($(digit), 32)
	DEFINE += -DLINUX32 -D__WORDSIZE=32
	LIB_DIR = -L/usr/bin -L/usr/lib -L/usr/local/lib -L/lib
else
	DEFINE += -DLINUX64 -D__WORDSIZE=64 -D__USE_FILE_OFFSET64 -D__USE_LARGEFILE64
	LIB_DIR = -L/usr/bin64 -L/usr/lib64 -L/usr/local/lib64 -L/lib64
endif
endif

ifeq ($(platform), linux)
	DYNAMIC_MODULE_EXT=lso	
else
#windows
	DYNAMIC_MODULE_EXT=dll
endif



ifeq ($(debug),on)
	DEBUG=-g
	DEFINE += -DDEBUG_PROJECT
else
	DEBUG=
	DEFINE += -DRELEASE_PROJECT
endif

ifeq ($(static),on)
	DEFINE += -DSTATIC_PROJECT
	EXE_FILE_TYPE=static
else
	EXE_FILE_TYPE=dynamic
endif


include $(HOME_DIR)/engine/engine.mk
include $(HOME_DIR)/testmodule/testmodule.mk
include $(HOME_DIR)/testmodule/testmoduleone/testmoduleone.mk

#######################################################################
#######################################################################
####################### TARGETS LIST ##################################
DLENGINE_DIR=$(HOME_DIR)/engine



ALL_TARGETS=\
	$(TARGETS_MAIN) \
	$(TARGETS_DLENGINE)\

#######################################################################
#######################################################################
#######################################################################

INCLUDE_DIR=\
	-I$(HOME_DIR) \
	-I$(ENGINE_DIR) \
	-I$(TEST_MODULE_DIR)\
	-I$(TEST_MODULE_ONE_DIR)



main:
	$(CC) $(DEBUG) $(CFLAGS) $(DEFINE) $(INCLUDE_DIR) -c $(HOME_DIR)/main.cpp -o $(BIN_DIR)/main.o

all: main $(TARGETS_ENGINE) $(TARGETS_TEST_MODULE) $(TARGETS_TEST_MODULE_ONE)
	$(CC) -W -Wall -ldl $(LFLAGS) $(LIB_DIR) $(ENGINE_DIR_BIN)/*.o $(TEST_MODULE_DIR_BIN)/*.o $(BIN_DIR)/main.o -o $(BIN_DIR)/main_test_$(EXE_FILE_TYPE)_$(PLATFORM_DIGIT)$(OUT_EXT)


.PHONY : all

 $(TARGETS_MAIN):
	$(CC) $(DEBUG) $(CFLAGS) $(DEFINE) $(INCLUDE_DIR) -c $(HOME_DIR)/$@.cpp -o $(BIN_DIR)/$@.o



########################################################################
########################################################################

# clean
clean:
	rm -rf $(ENGINE_DIR_BIN)/*.* $(TEST_MODULE_DIR_BIN)/*.* $(TEST_MODULE_ONE_DIR_BIN)/*.*

.PHONY : clean
		
		
help:
	@echo "***********************************************************"
	@echo "available options to build project:"
	@echo "all - build project"
	@echo "platform  - type of the platform. Available types: linux"
	@echo "digit - digit of the platform"
	@echo "debug information is on - debug=on"
	@echo "debug information is off - debug=off"
	@echo "example: make all platform=linux digit=64 debug=on"
	@echo "DEFAULT TARGETS for 'all' is platform=linux digit=64 debug=on" 
	@echo "***********************************************************"
